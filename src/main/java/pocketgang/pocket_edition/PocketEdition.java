package pocketgang.pocket_edition;

import net.fabricmc.api.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pocketgang.pocket_edition.block.PocketEditionBlocks;
import pocketgang.pocket_edition.entity.PocketEditionEntities;
import pocketgang.pocket_edition.item.PocketEditionItems;
import pocketgang.pocket_edition.villager.PocketEditionVillagers;

public class PocketEdition implements ModInitializer {
    public static final String ModID = "pocket_edition";
    public static final Logger LOGGER = LoggerFactory.getLogger(ModID);

    @Override
    public void onInitialize() {
        PocketEditionItems.registerItems();
        PocketEditionBlocks.registerBlocks();
        PocketEditionEntities.registerEntities();
        PocketEditionVillagers.registerVillagers();
        PocketEditionVillagers.registerTrades();
    }
}
