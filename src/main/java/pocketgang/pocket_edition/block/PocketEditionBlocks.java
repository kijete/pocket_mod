package pocketgang.pocket_edition.block;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.MapColor;
import net.minecraft.block.Material;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import pocketgang.pocket_edition.PocketEdition;

public class PocketEditionBlocks {
    public static final Block POCKET_SHRINE = new Block(FabricBlockSettings.of(Material.METAL, MapColor.BLACK));

    public static void registerBlocks() {
        Registry.register(Registry.BLOCK, new Identifier(PocketEdition.ModID, "pocket_shrine"), PocketEditionBlocks.POCKET_SHRINE);

        PocketEdition.LOGGER.info("Registered blocks");
    }
}
