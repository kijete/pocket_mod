package pocketgang.pocket_edition.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.rendering.v1.EntityModelLayerRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.minecraft.client.render.entity.model.EntityModelLayer;
import net.minecraft.util.Identifier;
import pocketgang.pocket_edition.PocketEdition;
import pocketgang.pocket_edition.entity.PocketEditionEntities;
import pocketgang.pocket_edition.entity.PocketEntityRenderer;

public class PocketEditionClient implements ClientModInitializer {
    public static final EntityModelLayer POCKET_MODEL_LAYER = new EntityModelLayer(new Identifier(PocketEdition.ModID, "pocket"), "main");

    @Override
    public void onInitializeClient() {
        EntityRendererRegistry.register(PocketEditionEntities.POCKET, (context) -> new PocketEntityRenderer(context));
        EntityModelLayerRegistry.registerModelLayer(POCKET_MODEL_LAYER, PocketEntityRenderer::getTexturedModelData);
    }
}
