package pocketgang.pocket_edition.entity;

import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import pocketgang.pocket_edition.PocketEdition;

public class PocketEditionEntities {
    public static final EntityType<PocketEntity> POCKET = Registry.register(Registry.ENTITY_TYPE,
            new Identifier(PocketEdition.ModID, "pocket"),
            FabricEntityTypeBuilder.create(SpawnGroup.CREATURE, PocketEntity::new).dimensions(EntityDimensions.fixed(0.6F, 0.7F))
                    .trackRangeChunks(8).fireImmune().build());

    public static void registerEntities() {
        FabricDefaultAttributeRegistry.register(PocketEditionEntities.POCKET, PocketEntity.createPocketAttributes());
        PocketEdition.LOGGER.info("Registered entities");
    }
}
