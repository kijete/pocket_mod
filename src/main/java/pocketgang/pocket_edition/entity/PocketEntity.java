package pocketgang.pocket_edition.entity;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityGroup;
import net.minecraft.entity.EntityStatuses;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.ParrotEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import pocketgang.pocket_edition.entity.goal.TeleportOwnerGoal;

public class PocketEntity extends TameableEntity {
    public PocketEntity(EntityType<? extends PocketEntity> entityType, World world) {
        super(entityType, world);
    }

    protected void initGoals() {
        this.goalSelector.add(1, new ModifyStrengthGoal(this));
        this.goalSelector.add(1, new SwimGoal(this));
        this.goalSelector.add(2, new PounceAtTargetGoal(this, 0.5F));
        this.goalSelector.add(3, new AttackGoal(this));
        this.goalSelector.add(4, new WanderAroundFarGoal(this, 0.8, 1.0000001E-5F));
        this.goalSelector.add(5, new LookAtEntityGoal(this, PlayerEntity.class, 10.0F));
        this.goalSelector.add(6, new TeleportOwnerGoal(this, 50.0F));

        this.targetSelector.add(1, new UntamedActiveTargetGoal(this, ParrotEntity.class, false, null));
        this.targetSelector.add(1, new ActiveTargetGoal(this, HostileEntity.class, false, null));
    }

    public static DefaultAttributeContainer createPocketAttributes() {
        return MobEntity.createMobAttributes()
                .add(EntityAttributes.GENERIC_MAX_HEALTH, 40.0)
                .add(EntityAttributes.GENERIC_ARMOR, 20.0)
                .add(EntityAttributes.GENERIC_ARMOR_TOUGHNESS, 12.0)
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0.5)
                .add(EntityAttributes.GENERIC_ATTACK_DAMAGE, 7000.0)
                .add(EntityAttributes.GENERIC_ATTACK_KNOCKBACK, 10.0).build();
    }

    // Make her less overpowered as a pet
    protected void onTamedChanged(){
        if (this.isTamed()) {
            this.getAttributeInstance(EntityAttributes.GENERIC_ATTACK_DAMAGE).setBaseValue(5.0);
            this.getAttributeInstance(EntityAttributes.GENERIC_ATTACK_KNOCKBACK).setBaseValue(1.0);
        } else {
            this.getAttributeInstance(EntityAttributes.GENERIC_ATTACK_DAMAGE).setBaseValue(7000.0);
            this.getAttributeInstance(EntityAttributes.GENERIC_ATTACK_KNOCKBACK).setBaseValue(10.0);
        }
    }

    public boolean handleFallDamage(float fallDistance, float damageMultiplier, DamageSource damageSource) {
        return false;
    }

    @Override
    public void onDeath(DamageSource damageSource) {
        if (!this.world.isClient && this.world.getGameRules().getBoolean(GameRules.SHOW_DEATH_MESSAGES) && this.getOwner() instanceof ServerPlayerEntity) {
            this.getOwner().sendMessage(Text.translatable("text.pocket_edition.pocket_left"));
        }
    }

    @Nullable
    @Override
    public PocketEntity createChild(ServerWorld world, PassiveEntity entity) {
        return null;
    }

    //todo temp
    public ActionResult interactMob(PlayerEntity player, Hand hand) {
        if (!this.world.isClient()) {
            this.setOwner(player);
            this.world.sendEntityStatus(this, EntityStatuses.ADD_POSITIVE_PLAYER_REACTION_PARTICLES);
            this.setPersistent();
        }
        return ActionResult.SUCCESS;
    }

    // Makes the Pocket strength relative to the owner's equipment
    private static class ModifyStrengthGoal extends Goal {
        private final TameableEntity tameable;
        private PlayerEntity owner;
        public  ModifyStrengthGoal(TameableEntity tameable) {
            this.tameable = tameable;
        }

        @Override
        public boolean canStart() {
            LivingEntity livingEntity = this.tameable.getOwner();
            boolean b = !(livingEntity instanceof PlayerEntity);
            if (livingEntity == null) {
                return false;
            } else if (livingEntity.isSpectator()) {
                return false;
            } else if (b) {
                return false;
            } else {
                this.owner = (PlayerEntity) livingEntity;
                return true;
            }
        }

        public boolean shouldContinue() { return false; }

        public void start() {
            final double[] damage = {5.0};
            this.owner.getInventory().main.iterator().forEachRemaining((itemStack -> {
                double itemAttackDamage = EnchantmentHelper.getAttackDamage(itemStack, EntityGroup.DEFAULT);
                if (itemAttackDamage * 0.75 > damage[0]) {
                    damage[0] = itemAttackDamage * 0.75;
                }
            }));
            tameable.getAttributeInstance(EntityAttributes.GENERIC_ATTACK_DAMAGE).setBaseValue(damage[0]);
        }
    }
}


