package pocketgang.pocket_edition.entity;

import net.minecraft.client.model.Dilation;
import net.minecraft.client.model.TexturedModelData;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.passive.CatEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3f;
import pocketgang.pocket_edition.client.PocketEditionClient;

import java.util.Iterator;
import java.util.List;

public class PocketEntityRenderer extends MobEntityRenderer<PocketEntity, PocketEntityModel<PocketEntity>> {
    public PocketEntityRenderer(EntityRendererFactory.Context context) {
        super(context, new PocketEntityModel(context.getPart(PocketEditionClient.POCKET_MODEL_LAYER)), 0.5F);
    }

    @Override
    public Identifier getTexture(PocketEntity entity) {
        return new Identifier("pocket_edition", "textures/entity/pocket/pocket.png");
    }

    public static TexturedModelData getTexturedModelData() {
        return TexturedModelData.of(PocketEntityModel.getModelData(Dilation.NONE), 64, 32);
    }

    protected void scale(PocketEntity pocketEntity, MatrixStack matrixStack, float f) {
        super.scale(pocketEntity, matrixStack, f);
        matrixStack.scale(0.8F, 0.8F, 0.8F);
    }
}
