package pocketgang.pocket_edition.item;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import pocketgang.pocket_edition.PocketEdition;
import pocketgang.pocket_edition.block.PocketEditionBlocks;
import pocketgang.pocket_edition.entity.PocketEditionEntities;

public class PocketEditionItems {
    public static final Item POCKET_SPAWN_EGG = new SpawnEggItem(PocketEditionEntities.POCKET, 0x261a0d, 0x734d26,
            new FabricItemSettings().group(ItemGroup.MISC));

    public static void registerItems() {
        Registry.register(Registry.ITEM, new Identifier(PocketEdition.ModID, "pocket_spawn_egg"), PocketEditionItems.POCKET_SPAWN_EGG);
        Registry.register(Registry.ITEM, new Identifier(PocketEdition.ModID, "pocket_shrine"), new BlockItem(PocketEditionBlocks.POCKET_SHRINE, new FabricItemSettings().group(ItemGroup.DECORATIONS)));
        PocketEdition.LOGGER.info("Registered items");
    }
}
