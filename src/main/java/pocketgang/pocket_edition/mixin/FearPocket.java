package pocketgang.pocket_edition.mixin;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.goal.FleeEntityGoal;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.mob.Monster;
import net.minecraft.entity.mob.PathAwareEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import pocketgang.pocket_edition.entity.PocketEntity;

@Mixin(HostileEntity.class)
public class FearPocket extends PathAwareEntity implements Monster {
    @Inject(at = @At("TAIL"), method="<init>")
    private void injectMethod(CallbackInfo info) {
        this.goalSelector.add(2, new FleeEntityGoal(this, PocketEntity.class, 12.0F, 1.0, 1.2));
    }

    // Never actually used
    protected FearPocket(EntityType<? extends PathAwareEntity> entityType, World world) {
        super(entityType, world);
    }
}
