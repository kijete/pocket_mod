package pocketgang.pocket_edition.villager;

import com.google.common.collect.ImmutableSet;
import net.fabricmc.fabric.api.object.builder.v1.trade.TradeOfferHelper;
import net.fabricmc.fabric.api.object.builder.v1.world.poi.PointOfInterestHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Identifier;
import net.minecraft.util.TimeSupplier;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryEntry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.village.TradeOffer;
import net.minecraft.village.VillagerProfession;
import net.minecraft.world.poi.PointOfInterestType;
import pocketgang.pocket_edition.PocketEdition;
import pocketgang.pocket_edition.block.PocketEditionBlocks;
import pocketgang.pocket_edition.item.PocketEditionItems;

import java.util.function.Predicate;

public class PocketEditionVillagers {
    public static PointOfInterestType POCKET_WORKSTATION;
    public static VillagerProfession POCKET_WORSHIPPER;

    public static void registerVillagers() {
        POCKET_WORKSTATION = PointOfInterestHelper.register(new Identifier(PocketEdition.ModID, "pocket_workstation"), 1, 1, PocketEditionBlocks.POCKET_SHRINE);
        POCKET_WORSHIPPER = registerProfession("pocket_worshipper", "pocket_workstation", SoundEvents.BLOCK_AMETHYST_BLOCK_CHIME);
        PocketEdition.LOGGER.info("Registered POIs and professions");
    }

    public static void registerTrades() {
        //todo add real trades probably with datapack
        TradeOfferHelper.registerVillagerOffers(POCKET_WORSHIPPER, 1, factories -> {
            factories.add(((entity, random) ->
                new TradeOffer(
                        new ItemStack(Items.EMERALD, 69),
                        new ItemStack(PocketEditionBlocks.POCKET_SHRINE, 1),
                        6, 2, 0f)
            ));
        });
        PocketEdition.LOGGER.info("Registered trades");
    }

    private static VillagerProfession registerProfession(String name, String poi_name, SoundEvent sound) {
        Predicate<RegistryEntry<PointOfInterestType>> predicate = holder -> holder.matchesKey(RegistryKey.of(Registry.POINT_OF_INTEREST_TYPE_KEY, new Identifier(PocketEdition.ModID, poi_name)));
        
        return Registry.register(Registry.VILLAGER_PROFESSION,
                new Identifier(PocketEdition.ModID, name),
        new VillagerProfession(name, predicate, predicate,
                ImmutableSet.of(), ImmutableSet.of(), sound));
    }
}
